<h1>Sistema Teste</h1>
<hr>
<?php

echo 'Versão: ' . getVersao('teste');

function getVersao($sistema)
{
    $pasta = $sistema;
    $versao = "";
    exec("cd " . $_SERVER['DOCUMENT_ROOT'] . "/" . $pasta . " && git rev-parse --abbrev-ref HEAD", $branchName);
    $branchName = current($branchName);
    if ($branchName == "master") {
        $versao = "Master";
    } elseif ($branchName == "HEAD") {
        exec("cd " . $_SERVER['DOCUMENT_ROOT'] . "/" . $pasta . " && git describe --tags --abbrev=0", $tagName);
        $versao = array_pop($tagName);
    } else {
        $versao = $branchName;
    }
    return $versao;
}

?>